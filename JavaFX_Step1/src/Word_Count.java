import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by nourhalabi on 09/03/16.
 */
public class Word_Count {

    private Map<String,Double> wordCounts;
    private Map<String,Double> tempCount;
    private double sizeofMap;
    private Map<String, Double> TestingProbability;
    private Map<String, Double> TrainingProbability = new TreeMap<>();
    ObservableList<TestFile> TestFilesSpam = FXCollections.observableArrayList();
    ObservableList<TestFile> TestFilesHam = FXCollections.observableArrayList();

   /* public static ObservableList<TestFile> getTestFilesSpam() {
        return TestFilesSpam;
    }

    public static ObservableList<TestFile> getTestFilesHam() {
        return TestFilesHam;
    }

   */

    public Word_Count() {
        wordCounts = new TreeMap<>();
        tempCount = new TreeMap<>();
        sizeofMap = 0;
    }

    public double getSizeofMap() {
        return sizeofMap;
    }


    public Map<String, Double> getWordCounts() {
        return wordCounts;
    }


    public void processFile(File file) throws IOException {
        if (file.isDirectory()) {
            //if(file.)

            // process all of the files recursively
            File[] filesInDir = file.listFiles();


            for (int i = 0; i < filesInDir.length; i++) {
                processFile(filesInDir[i]);

            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);
            tempCount.clear();
            while (scanner.hasNext()) {

                String word = scanner.next();

                if (isWord(word)) {
                    countWord(word);

                }
            }
        }
    }

    private void countWord(String word) {
        if(!(tempCount.containsKey(word))) {
            if (wordCounts.containsKey(word)) {

                double oldCount = wordCounts.get(word);
                wordCounts.put(word, oldCount + 1);

            } else {
                wordCounts.put(word, 1.0);
                sizeofMap +=1;
            }
            tempCount.put(word,1.0);
        }

    }

    public boolean isWord(String str){
        String pattern = "^[a-zA-Z]*$";
        if (str.matches(pattern)){
            return true;
        }
        return false;
    }

    public void printWordCounts(int minCount, File outputFile) throws FileNotFoundException {
        System.out.println("Saving word counts to " + outputFile.getAbsolutePath());
        if (!outputFile.exists() || outputFile.canWrite()) {
            PrintWriter fout = new PrintWriter(outputFile);

            Set<String> keys = wordCounts.keySet();
            Iterator<String> keyIterator = keys.iterator();

            while(keyIterator.hasNext()) {
                String key = keyIterator.next();
                double count = wordCounts.get(key);

                if (count >= minCount) {
                    //System.out.println(key + ": " + count);
                    fout.println(key + ": " + count);
                }
            }
            fout.close();
        } else {
            System.err.println("Cannot write to output file");
        }
    }

  /*  public void processTestingFileSpam(File file) throws IOException {
        //System.out.println("I'm here.");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();

            for (int i = 0; i < filesInDir.length; i++) {
                processTestingFileSpam(filesInDir[i]);
            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);
            double n_value = 0;
            String word = " ";
            TestFile spamTestFile = new TestFile(file.getName(), 0.0, "spam");
            while (scanner.hasNext()) {

                word = scanner.next();

                if (isWord(word))
                if (TrainingProbability.containsKey(word)) {
                //
                    System.out.println(word);

                    //_____calculations for n_____//

                    double probability = TrainingProbability.get(word);
                    if (probability != 0 || probability != 1) {

                        n_value += Math.log(1 - probability) - Math.log(probability);
                        System.out.println(n_value);
                    } else {

                    }
                    }
                    // calculateTestSpam(scanner, n_value, TrainingProbability);

                    //}

              //  }

            }

            //_____calculations for 1/(1 + e^n)_____//
            double count_testing_value = 1 / (1 + (Math.pow(Math.E, n_value)));
            System.out.println(word + " " +count_testing_value);

        }
    }
    */

    public void getProbabilityTraining(Map<String, Double> TrainProbability)
    {
        TrainingProbability = TrainProbability;
    }


/*
    public void processTestingFileSpam(File file, Map<String, Double> TrainingProbability) throws IOException {
        //System.out.println("I'm here.");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();

            for (int i = 0; i < filesInDir.length; i++) {
                processTestingFileSpam(filesInDir[i], TrainingProbability);
            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);
            double n_value = 0;
            while (scanner.hasNext()) {

                String word = scanner.next();
               // System.out.println(word);

                if (isWord(word)) {


                    //if (TrainingProbability.containsKey(word)) {

                        //_____calculations for n_____//

                        double probability = TrainingProbability.get(word);
                        if (probability != 0 && probability != 1) {

                            n_value += Math.log(1 - probability) - Math.log(probability);
                            System.out.println(n_value);
                        } else {
                        }

                       // calculateTestSpam(scanner, n_value, TrainingProbability);

                    //}

                }

            }

        }
    }
*/
    void calculateTestProbability(Scanner scanner)
    {


    }

    public void calculateTestSpam(Scanner scanner, double n_value, Map<String, Double> TrainingProbability ) {

        TestingProbability = new TreeMap<>();

        //_____calculations for 1/(1 + e^n)_____//
        double count_testing_value = 1 / (1 + (Math.pow(Math.E, n_value)));

        while (scanner.hasNext()) {
            String word = scanner.next();

            if (isWord(word)) {
                if (TrainingProbability.containsKey(word)) {
                    //save values in Pr(S|F) map
                    TestingProbability.put(word, count_testing_value);
                   // System.out.println(word + " " + count_testing_value);


                }

            }
        }
    }







   /* public static void main(String[] args) {
        if (args.length < 2) {
            System.err.println("Usage:  java WordCounter <dir> <outputFile>");
            System.exit(0);
        }

        Word_Count wordCount = new Word_Count();
        File dataDir = new File("/home/nourhalabi/IdeaProjects/Assignment_1/data/");
        System.out.println("File: " + dataDir);

        try {
            wordCount.processFile(dataDir);
            wordCount.printWordCounts(2, new File(args[1]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        */
    }



