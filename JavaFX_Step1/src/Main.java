import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.*;
import javafx.scene.input.KeyCombination;
import javafx.scene.image.*;
import javafx.collections.*;
import javafx.event.*;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.cell.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

public class Main extends Application {
    private Stage window;
    private BorderPane layout;
    private TableView table;
    private TextField unField, passField, phoneField,fullNameField, emailField;
    private DatePicker dateField;

    private Map<String,Double> SpamMap;
    private Map<String,Double> HamMap;
    private Map<String,Double> SpamMapTest;
    private Map<String,Double> HamMapTest;
    private Map<String, Double> ProbabiltyMap;
    private double sizeofSpamMap;
    private double sizeofHamMap;
    Word_Count wordCount_STest;
    Word_Count wordCount_HTest;
    File SpamTestDir = new File("/home/nourhalabi/IdeaProjects/Assignment_1/data/test/spam");
    File HamTestDir;

    @Override
    public void start(Stage primaryStage) throws Exception {
       primaryStage.setTitle("Assignment 1");
        //_____Directory Setup_____//
        DirectoryChooser directoryChooser = new DirectoryChooser();
        directoryChooser.setInitialDirectory(new File("."));
        File mainDirectory = directoryChooser.showDialog(primaryStage);


        //_____TRAINING_____//
        SpamMap = new TreeMap<>();
        HamMap = new TreeMap<>();

        Word_Count wordCount_Spam = new Word_Count();
        Word_Count wordCount_Ham = new Word_Count();

        File SpamDir = new File(mainDirectory.getPath() + "/train/spam/");
        File HamDir = new File(mainDirectory.getPath() + "/train/ham");
        System.out.println("Spam File: " + SpamDir);
        System.out.println("Ham File: " + HamDir);

        try {
            wordCount_Spam.processFile(SpamDir);
          //  wordCount_Spam.printWordCounts(2, new File("/home/nourhalabi/IdeaProjects/Assignment_1/data/output_spam2"));

            wordCount_Ham.processFile(HamDir);
          //  wordCount_Ham.printWordCounts(2, new File("/home/nourhalabi/IdeaProjects/Assignment_1/data/output_ham2"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        HamMap = wordCount_Ham.getWordCounts();
        sizeofHamMap = wordCount_Ham.getSizeofMap();
      //  System.out.println("ham size: " + sizeofHamMap);
        SpamMap = wordCount_Spam.getWordCounts();
        sizeofSpamMap = wordCount_Spam.getSizeofMap();
       // System.out.println("spam size: " + sizeofSpamMap);

        calculateFrequencies();

        //_____TESTING_____//
        wordCount_STest = new Word_Count();
        wordCount_HTest = new Word_Count();
        SpamTestDir = new File(mainDirectory.getPath() + "/test/spam/");
        HamTestDir = new File(mainDirectory.getPath() + "/test/ham");
        System.out.println("Spam Test File: " + SpamTestDir);
        System.out.println("Ham Test File: " + HamTestDir);

        wordCount_HTest.getProbabilityTraining(ProbabiltyMap);


        try {
           // processTestingFileSpam(SpamTestDir); //processFile(SpamTestDir);
            //  wordCount_Spam.printWordCounts(2, new File("/home/nourhalabi/IdeaProjects/Assignment_1/data/output_spam2"));

            wordCount_HTest.processFile(HamTestDir);
            //  wordCount_Ham.printWordCounts(2, new File("/home/nourhalabi/IdeaProjects/Assignment_1/data/output_ham2"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        SpamMapTest = new TreeMap<>();
        HamMapTest = new TreeMap<>();
        SpamMapTest = wordCount_Spam.getWordCounts();
        HamMapTest = wordCount_Ham.getWordCounts();


        // calculateTesting();




        //_____CREATE TABLE_____//
        table = new TableView();

        //table.setItems(getTestFilesSpam());
        //table.setItems(get);
        //table.setEditable(true);


/*
        TableColumn SID = new TableColumn("SID");
        SID.setMinWidth(100);
        TableColumn Assignments = new TableColumn("Assignments");
        Assignments.setMinWidth(100);
        TableColumn Midterms = new TableColumn("Midterms");
        Midterms.setMinWidth(100);
        TableColumn FinalE = new TableColumn("Final Exam");
        FinalE.setMinWidth(100);
        TableColumn FinalM = new TableColumn("Final Mark");
        FinalM.setMinWidth(100);
        TableColumn LetterG = new TableColumn("Letter Grade");
        LetterG.setMinWidth(100);



        SID.setCellValueFactory(
                new PropertyValueFactory<StudentRecord, String>("studentID"));
        Assignments.setCellValueFactory(
                new PropertyValueFactory<StudentRecord, Float>("assignments"));
        Midterms.setCellValueFactory(
                new PropertyValueFactory<StudentRecord, String>("midterm"));
        FinalE.setCellValueFactory(
                new PropertyValueFactory<StudentRecord, String>("finalExam"));
        FinalM.setCellValueFactory(
                new PropertyValueFactory<StudentRecord, String>("finalMark"));
        LetterG.setCellValueFactory(
                new PropertyValueFactory<StudentRecord, String>("letterGrade"));
*/


       // table.setItems(DataSource.getAllMarks());
       // table.getColumns().addAll(SID,Assignments,Midterms,FinalE,FinalM,LetterG);

       // final VBox vbox = new VBox();
       // vbox.setSpacing(5);
       // vbox.setPadding(new Insets(10, 0, 0, 10));
       // vbox.getChildren().addAll(table);

        BorderPane layout = new BorderPane(); //can dock things in S, E, W, N
       // layout.setCenter(table);

        Scene scene = new Scene(layout, 600, 600);
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    void calculateFrequencies() {
        //______calculate the probability that a word appears in a ham file_____//
        Map<String, Double> HamProbabilty = new TreeMap<>();
        double HamCalculation = 0;
        double Ham_Prob_Count = 0;
        double Spam_Prob_Count = 0;

        //_____Setup iterators for Ham_____//
        Set<String> keys2 = HamMap.keySet();
        Iterator<String> keyIterator2 = keys2.iterator();
        String key2 = "";

        //iterate through he map to retrieve count and key values
        while (keyIterator2.hasNext()) {
            key2 = keyIterator2.next();
            double count2 = HamMap.get(key2);

            HamCalculation = count2 / sizeofHamMap;
            HamProbabilty.put(key2, HamCalculation);
        }

        System.out.println("ham file frequency probability calculated");
        //_____calculate the probability that a word appears in a spam file_____//
        Map<String, Double> SpamProbabilty = new TreeMap<>();
        double SpamCalculation = 0;

        Set<String> keys = SpamMap.keySet();
        Iterator<String> keyIterator = keys.iterator();
        String key = "";
        while (keyIterator.hasNext()) {
            key = keyIterator.next();
            double count = SpamMap.get(key);

            SpamCalculation = count / sizeofSpamMap;
            SpamProbabilty.put(key, SpamCalculation);

        }
        System.out.println("Ham and Spam map calculated");
        calculateProbability(SpamProbabilty, HamProbabilty);

    }

    void calculateProbability(Map<String, Double> SpamFrequency,  Map<String, Double> HamFrequency){
        //_____Iterate through map to get the words_____//
        Set<String> keys2 = HamMap.keySet();
        Iterator<String> keyIterator2 = keys2.iterator();
        String word = "";

        //iterate through the map to retrieve word
        while (keyIterator2.hasNext()) {
            word = keyIterator2.next();

            //_____calculate the probability that a file is spam_____//
            ProbabiltyMap = new TreeMap<>();
            double Ham_Prob_Count = 0;
            double Spam_Prob_Count = 0;
            if (HamFrequency.containsKey(word)) { //if word exists in map
                Ham_Prob_Count = HamFrequency.get(word);
            }
            else{
                Ham_Prob_Count = 0.00001;
            }

            if (SpamFrequency.containsKey(word)) { //if word exists in map
                Spam_Prob_Count = SpamFrequency.get(word);
            }
            else{
                Spam_Prob_Count = 0.00001;
            }

            double Probability = Spam_Prob_Count/ (Spam_Prob_Count + Ham_Prob_Count);
            ProbabiltyMap.put(word, Probability);

            //calculateTesting(word, Probability);
            //System.out.println(word + ": " + Probability);


            try {

                processTestingFileSpam(SpamTestDir, ProbabiltyMap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

    }


    public void processTestingFileSpam(File file, Map<String, Double> ProbabiltyMap) throws IOException {
        //System.out.println("I'm here.");
        if (file.isDirectory()) {
            // process all of the files recursively
            File[] filesInDir = file.listFiles();

            for (int i = 0; i < filesInDir.length; i++) {
                processTestingFileSpam(filesInDir[i], ProbabiltyMap);
            }
        } else if (file.exists()) {
            // load all of the data, and process it into words
            Scanner scanner = new Scanner(file);
            double n_value = 0;

            TestFile spamTestFile = new TestFile(file.getName(), 0.0, "spam");
            while (scanner.hasNext()) {

                String word = scanner.next();

                if(wordCount_STest.isWord(word))
                {
                    if (ProbabiltyMap.containsKey(word)) {
                        System.out.println(word);

                    }
                }
            }
        }
    }
/*
                if (wordCount_STest.isWord(word))
                    if (ProbabiltyMap.containsKey(word)) {
                        //
                        System.out.println(word);

                        //_____calculations for n_____//

                        double probability = ProbabiltyMap.get(word);
                        if (probability != 0 || probability != 1) {

                            n_value += Math.log(1 - probability) - Math.log(probability);
                            System.out.println(n_value);
                        } else {

                        }
                    }
                // calculateTestSpam(scanner, n_value, TrainingProbability);

                //}

                //  }

            }

            //_____calculations for 1/(1 + e^n)_____//
            //double count_testing_value = 1 / (1 + (Math.pow(Math.E, n_value)));
           // System.out.println(word + " " +count_testing_value);
         //   System.out.println(n_value);

        }
        */


    public static void main(String[] args) {
        launch(args);

    }
}
